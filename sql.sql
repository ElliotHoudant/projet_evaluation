-- !preview conn=DBI::dbConnect(RSQLite::SQLite())

-- Table des patients
CREATE TABLE Patients (
    ID INT PRIMARY KEY,
    SEXE INT,
    POIDS DECIMAL(5,2),
    DDN DATE,
    AGE INT,
    PREMA INT,
    PATHO VARCHAR(255)
);

-- Table du passage au bloc et en réanimation
CREATE TABLE PassageBlocRea (
    ID INT PRIMARY KEY,
    ID_Patient INT,
    DDC DATETIME,
    SAT_PO DECIMAL(5,2),
    CEC VARCHAR(50),
    CLAMPAGE VARCHAR(50),
    CARDIOPLEGIE VARCHAR(50),
    TEMP_CEC DECIMAL(4,2),
    VENTIL INT,
    FEVG_ETO INT,
    FEVD_ETO INT,
    SHUNT_ETO INT,
    THORAX INT,
    ECMO_PO INT,
    FOREIGN KEY (ID_Patient) REFERENCES Patients(ID)
);

-- Table des mesures post-opératoires
CREATE TABLE MesuresPostOperatoires (
    ID INT PRIMARY KEY,
    ID_Patient INT,
    PH_A DECIMAL(3,2),
    PCO2_A DECIMAL(5,2),
    PO2_A DECIMAL(5,2),
    LACTATES_A DECIMAL(5,2),
    PH_V DECIMAL(3,2),
    PCO2_V DECIMAL(5,2),
    PO2_V DECIMAL(5,2),
    LACTATES_V DECIMAL(5,2),
    SCVO2 DECIMAL(5,2),
    PVC DECIMAL(5,2),
    TEMP DECIMAL(4,2),
    TROPO DECIMAL(5,2),
    BNP DECIMAL(5,2),
    HB DECIMAL(5,2),
    PLQ DECIMAL(5,2),
    FIBRI DECIMAL(5,2),
    DIURESE DECIMAL(5,2),
    NIRS_R INT,
    NIRS_C INT,
    PM INT,
    FEVG_ETT INT,
    FEVD_ETT INT,
    REPRISE_CHIR INT,
    REPRISE_ECMO INT,
    FOREIGN KEY (ID_Patient) REFERENCES Patients(ID)
);

-- Table des administrations de médicaments
CREATE TABLE AdministrationsMedicaments (
    ID INT PRIMARY KEY,
    ID_Patient INT,
    NAD INT,
    ADRE INT,
    MILRI INT,
    LASILIX INT,
    TRANSFU_CGR INT,
    TRANSFU_PLQ INT,
    TRANSFU_PFC INT,
    TRANSFU_FIBRI INT,
    FOREIGN KEY (ID_Patient) REFERENCES Patients(ID)
);

-- Question 1
SELECT SEXE, COUNT(*) AS NombrePatients
FROM Patients
GROUP BY SEXE;


-- Question 2
SELECT
    YEAR(DDC) AS Annee,
    COUNT(*) AS NombrePatients,
    AVG(DATEDIFF(MINUTE, DDC, DATEADD(day, 1, DDC))) AS DureeMoyenneCEC,  -- Durée moyenne de CEC en minutes
    AVG(SAT_PO) AS SaturationMoyenne,
    STDEV(SAT_PO) AS EcartTypeSaturation
FROM
    PassageBlocRea
GROUP BY
    YEAR(DDC);

-- Question 3

SELECT
    ID_Patient,
    MIN(PH_V) AS MinPHVeineux,
    MAX(PH_V) AS MaxPHVeineux
FROM
    MesuresPostOperatoires
GROUP BY
    ID_Patient;

-- Question 4

SELECT
    COUNT(*) AS NombrePatientsAvecNoradrenaline,
    SUM(CASE WHEN NAD != 0 THEN 1 ELSE 0 END) AS NombrePatientsAvecNoradrenaline,
    COUNT(*) AS NombrePatientsAvecAdrenaline,
    SUM(CASE WHEN ADRE != 0 THEN 1 ELSE 0 END) AS NombrePatientsAvecAdrenaline
FROM
    AdministrationsMedicaments;
    
    

-- Question 5

SELECT
    COUNT(*) AS NombrePatientsSansNoradrenalineNiAdrenaline
FROM
    AdministrationsMedicaments
WHERE
    NAD = 0 AND ADRE = 0;
